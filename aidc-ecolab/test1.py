'''
Utility functions. Hopefully reusable.
'''
import numpy as np
import pandas as pd
import warnings
from itertools import chain
from collections import Iterable
import re
import jellyfish
from sklearn.pipeline import Pipeline

# Tiny helper functions
def remove_prefix(iterable, prefix):
    """Function that removes the prefix for every element in iterable if prefix is present"""
    return [re.sub(pattern = '^%s' % prefix, string = element, repl = '') for element in iterable]

def get_first_item(iterable, default = None):
    """Function that returns the first value of an iterable / input if input is not iterable
    """
    if isinstance(iterable, Iterable):
        return next(iter(iterable), default)
    return iterable

def every_a_in_b(a,b):
    """Returns True if every element of first iterable is present in second iterable

    Parameters
    ----------
    a: array-like
        list of elements
    b: array-like
        another list of elements
    
    Returns
    -------
    result: boolean
        Returns True if every element of a is present in b
    """
    return set(a).issubset(set(b))

########################
# Standard Dictionaries#
########################

def load_dictionary(filename):
    """Function that loads data from a CSV & returns a dict that contains key-value pairs
    as specified

    Parameters
    ----------
    filename: str
        Name of the CSV file that contains tabular data. A 'Key' column is mandatory.

    Returns
    -------
    result: dict
        A dictionary of dictionaries
    """
    dictionary_data = pd.read_csv(filename)
    assert('Key' in dictionary_data.columns)
    distinct_records = dictionary_data.drop_duplicates(subset = 'Key')

    if len(distinct_records) < len(dictionary_data):
        warnings.warn("%s duplicates found in dictionary. Dropping." % (len(dictionary_data) - len(distinct_records)), RuntimeWarning)
        
    return distinct_records.set_index('Key').to_dict(orient = 'index')

###################
# Data Processing #
###################

def get_nth_value_from_row(pandas_df, index_array):
    """
    Gets an array of the elements of the rows in a pandas DataFrame based on the indices specified in index_array

    Parameters
    ----------
    pandas_df: pandas.DataFrame
        Input DataFrame
    index_array: list
        list that contains indexes for each row in the dataframe
    
    Example
    -------
    >>> df = pd.DataFrame({'A': 1.,
   ...:                     'B': pd.Timestamp('20130102'),
   ...:                     'C': pd.Series(1, index=list(range(4)), dtype='float32'),
   ...:                     'D': np.array([3] * 4, dtype='int32'),
   ...:                     'E': pd.Categorical(["test", "train", "test", "train"]),
   ...:                     'F': 'foo'})
    >>> get_nth_value_from_row(df, [0,1,2,2,5])
    array([1.0, Timestamp('2013-01-02 00:00:00'), 1.0, 'foo'], dtype=object)
    """
    # Length of index_array must be equal to number of rows. Index must not be out of bound
    assert(len(pandas_df) == len(index_array) and max(index_array) < pandas_df.shape[1])
    index_array = list(index_array) # Casting to list
    # TODO: Check if np.choose does not scale. https://stackoverflow.com/a/44121293
    # The official docs don't mention the limitation https://docs.scipy.org/doc/numpy/reference/generated/numpy.choose.html 
    return np.choose(index_array, pandas_df.values.T)

def choose_most_similar_value(pandas_df, value_column, cols_to_compare, similarity_func = jellyfish.jaro_winkler):
    """
    Chooses the most similar values to the value_column from the list of columns to compare with and returns the same

    Parameters
    ----------
    pandas_df: pandas.DataFrame
        Input DataFrame
    value_column: str
        Name of the field to which other columns must be compared
    cols_to_compare: list/ array-like
        Collection of names of columns that ought to be compared with value_column
    similarity_func: function / optional
        Symmetric string similarity function
    
    Returns
    -------
    result: numpy.array
        An array that contains the most similar values
    """
    list_of_pairs = [[value_column, other_column] for other_column in cols_to_compare]
    indices_of_max_sim = generate_similarity_array(pandas_df, list_of_pairs = list_of_pairs, similarity_func = similarity_func).argmax(axis=1)
    return get_nth_value_from_row(pandas_df[cols_to_compare], indices_of_max_sim)

def similar_cols(series1, series2, similarity_func = jellyfish.jaro_winkler):
    """Function that returns the simlarities between that correponding elements of two Pandas series.

    Parameters
    ----------
    series1: pandas.Series
        First pandas series
    series2: pandas.Series
        Second pandas series
    similarity_func: function, optional
        Symmetric string similarity function

    Returns
    -------
    result: list
        list of similarities
    """
    return [similarity_func(str(x).upper(),str(y).upper()) for x,y in zip(series1.fillna(''),series2.fillna(''))]

def generate_similarity_array(pandas_df, list_of_pairs, similarity_func = jellyfish.jaro_winkler):
    """Function that returns an array that contains the similarities between the specified sets of columns

    pandas_df: pandas.DataFrame
        Input pandas dataframe
    list_of_pairs: list(tuples)
        A list of tuples. Each tuple contains a pair of columns present in pandas_df whose similarities
        will be calculated. Eg: [('col1','col2'),('col1','col3')]
    similarity_func: function, optional
        Symmetric string similarity function
    
    Returns
    -------
    result: numpy.array
        array that contains similarities
    """
    # Check if the columns specified is present in pandas_df
    # FIXME: every_a_in_b might have a bug
    # assert(every_a_in_b(flatten(list_of_pairs), pandas_df.columns))
    raw_results = [similar_cols(pandas_df[column1], pandas_df[column2], similarity_func) for column1, column2 in list_of_pairs]
    result = np.array(raw_results).T
    return result

def generate_query_dictionaries(pandas_df, derived_column = None, columns = None):
    """Generates a list of dictionaries that will be sent to the APIs

    Parameters
    ----------
    pandas_df: pandas.DataFrame
        Input pandas dataframe from which list of dictionaries is generated
    derived_column: str, optional
        The derived column that is a combination of multiple columns 
    columns: list
        list of columns to concatenate so that a search_term can be derived
    
    Returns
    -------
    result: list
        list of dictionaries
    """
    temp_df = pandas_df.copy()

    if derived_column and columns:
        assert(every_a_in_b(columns, temp_df.columns))
        temp_df[derived_column] = concatenate_columns(temp_df,columns)
    return temp_df.to_dict(orient = 'records')


def iso_country(country_name,dictionary, output_type = 'ISO3A'):
    """Function that standardizes the country name provied & returns and standard version

    Parameters
    ----------
    country_name: str
        The name of the country from the dataset
    dictionary: dict/ dict-like
        A dict that contains the mapping from different strings to the standard version.Eg:-
        {
            'UNITED STATES MINOR OUTLYING ISL': {'ISO3A': 'USA',
                                                'Country': 'UNITED STATES OF AMERICA',
                                                'Type': 'Variant'},
            'UNITED STATES': {'ISO3A': 'USA',
                            'Country': 'UNITED STATES OF AMERICA',
                            'Type': 'Variant'},
            'UNITED STATED': {'ISO3A': 'USA',
                            'Country': 'UNITED STATES OF AMERICA',
                            'Type': 'Misspelling'}
        }
    output_type: optional, {'ISO3A', 'Country'}
        Type of output to be returned. 'CAN' is the ISO3A version of CANADA
    
    Returns
    -------
    result: str
        The standard version of the string or the original string (UPPERCASE) if not present in dictionary
        TODO: Write to log file if country is not present
    """
    assert(output_type in {'ISO3A', 'Country'})
    country_name = country_name.upper()
    if country_name in dictionary:
        return dictionary[country_name][output_type]
    return country_name

def flatten(list_of_lists):
    """Returns a flattened list from a list of lists"""
    return list(chain.from_iterable(list_of_lists))

def flatten_if_list(objects):
    """Returns a list that contains all the strings in the list & also the strings within a nested list.
    Order is *not* preserved.

    Parameter
    --------
    objects: array-like
        A list of strings / a list of lists or a composite

    Examples
    --------
    >>> flatten_if_list([['foo','bar'],'baz'])
    ['foo', 'bar', 'baz']

    >>> flatten_if_list(['baz',['foo','bar']])
    ['foo', 'bar', 'baz']

    >>> flatten_if_list(['baz','foo','bar'])
    ['baz', 'foo', 'bar']    
    """
    elements_from_lists = flatten([element for element in objects if isinstance(element, list)])
    other_elements = [element for element in objects if not isinstance(element, list)]
    return elements_from_lists + other_elements

def detect_extension(file_name):
    """Returns the extension of a filename (str). Raises RuntimeWarning if length of extension
    is greater than 4"""
    *_, extension = file_name.split('.')
    extension = extension.lower()

    if len(extension) > 4:
        warnings.warn("Length of extension greater than 4.",RuntimeWarning)
    return extension

def read_data(file_name, skip_rows = None, sheet_name = None, encoding = 'utf8'):
    '''
    Reads data from a file and returns a Pandas DataFrame.

    Parameters
    ----------
    file_name: str
        the name of the file to be read
    skip_rows: int, optional
        specify the number of rows to skip
    sheet_name: str, optional
        the name of sheet to be read from the Excel workbook. If sheet_name is not present
        the function will consolidate all sheets
    encoding: {'utf8','latin1'}
        the encoding of the CSV file
        
    Returns
    -------
    pandas.DataFrame

    Raises
    ------
    ValueError
        when the extension is unsupported
    '''
    extension = detect_extension(file_name)
    # Check if Excel workbook
    if extension in {'xls','xlsx'}:
        if not sheet_name:
            # Multiple sheets are to be consolidated
            workbook = pd.ExcelFile(file_name) #excel object would be created to use method of it
            SHEET_ID = 'SheetName'
            consolidated = pd.DataFrame() #blank panda dataframe
            for sheet in workbook.sheet_names: #retrieve and iterate over all the sheets in present in the excel sheet
                sheet_data = pd.read_excel(file_name,sheet_name=sheet, skiprows=skip_rows) #retriving excel data in panda dataframe format
                sheet_data[SHEET_ID] = sheet #in SheetName storing particular sheet name
                consolidated = consolidated.append(sheet_data) #appending all sheets data into a single dataframe
            return consolidated
        else:
            return pd.read_excel(file_name, sheet_name= sheet_name, skiprows= skip_rows) #if only single sheet name present then return direct formatted dataframe
    elif extension == 'csv':
        return pd.read_csv(file_name, encoding = encoding, skiprows= skip_rows) #if csv file just use different method of pd
    else:
        raise ValueError("Extension: %s is not supported." % extension) #if not xls/xlsx/csv then raise error

def write_data(pandas_df, file_name):
    """Writes a Pandas DataFrame to an Excel workbook/ CSV file based on the 
    extension in the filename"""
    extension = detect_extension(file_name)

    if extension in {'xls','xlsx'}:
        pandas_df.to_excel(file_name)
    elif extension == 'csv':
        pandas_df.to_csv(file_name)
    else:
        raise ValueError('Unsupported extension: %s.' % extension)

def flatten_json(y):
    """Flattens a JSON (dict/list) and returns a dict."""
    out = {}

    def flatten(x, name=''):
        if type(x) is dict:
            for a in x:
                flatten(x[a], name + a + '_')
        elif type(x) is list:
            i = 0
            for a in x:
                flatten(a, name + str(i) + '_')
                i += 1
        else:
            out[name[:-1]] = x

    flatten(y)
    return out

def prefix_keys(dictionary, prefix):
    """Returns a dict that is a version of the input dict that contains modified keys
    with the specified prefix

    Parameters
    ----------
    dictionary: dict
        input dictionary
    
    prefix: str
        prefix that required for the keys

    Returns
    -------
    dict
    """
    return {prefix + key: value for key, value in dictionary.items()}

###################
# String functions#
###################

def standardize_string(string, dictionary, key):
    """Function that standardizes words in a string based on a dictionary
    
    Parameters
    ----------
    string: str
        Input string. Eg:- 221B Baker St
    
    dictionary: dict
        Expects a dictionary of dictionaries. The keys are the words to be modified. Eg:-
        {'rd': {'Standard Form': 'road', 'Identifier': 1}, 'st' :{'Standard Form': 'street', 'Identifier': 2}}
    key: str
        Name of the field in the inner dict that has the standard string. Eg:- 'Standard Form'
    
    Retruns
    -------
    string: str
        String that is standardized

    Raises
    ------
    RuntimeWarning:
        If the provided string is not an instance of str    
    """
    if isinstance(string, str):
        word_list = []
        for word in string.split():
            if word in dictionary:
                word_list.append(dictionary[word][key])
            else:
                word_list.append(word)
        return ' '.join(word_list)
    else:
        warnings.warn("Input to standardize_string is not a string.", RuntimeWarning)
        return string

def suffix_items(iterable, suffix):
    '''Inserts a suffix after a string/every string element in an iterable.

    Parameters
    ----------
    iterable: str, array-like
        input iterable/ string
    suffix: str
        the suffix for each string
    
    Returns
    -------
    result: str, list
    '''
    if isinstance(iterable, str):
        return iterable + suffix
    elif isinstance(iterable, Iterable):
        return [base_string + suffix for base_string in iterable]
    else:
        raise TypeError('Object of type %s is not supported.' % type(iterable))

def replace_string(text, del_list):
    '''Deletes all the words from the text that are contained in del_list

    Parameters
    ----------
    text: str
        Input string from which substrings are to be deleted
    del_list: list
        List of strings that ought to be removed from text

    Example
    -------
    >>> replace_string('To be or not be foo. That is the bar question', ['foo', 'bar'])
    'To be or not be . That is the  question'

    Returns
    -------
    result: str
    '''
    text = str(text)
    return re.sub('|'.join(del_list),'',text)

def string_containment(string1, string2):
    """Checks if one string is contained in other and also if the length of contained string is 
    atleast 40% of that of the longer string. Returns a boolean.
    """
    short_string, long_string = sorted([string1, string2], key=len)
    return long_string and short_string and (short_string in long_string) and (len(short_string) >= 0.4 * len(long_string))

def concatenate_strings(iterable, delimiter, distinct = False, similarity_func = jellyfish.jaro_winkler):
    """Returns a concatenated string using the elememnts from the iterable
    delimited by the delimiter after ignoring nulls.

    Parameters
    ----------
    iterable: array-like
        This object contains the array of strings that ought to be concatenated
    delimiter: str
        The string that delimits the concatenated output
    distinct: boolean, default: False
        Concatenates only distinct strings in the iterable if set to True. Currently works for only two strings.
        Note: This flag is specifically used to solve Ecolab's problem of identical names in both Acc Name 1 & Acc Name 2
    similarity_func: function, optional
        The similarity function that is used to determine whether two strings are the same
    
    Returns
    -------
    result: str

    Examples
    --------
    """
    if distinct:
        assert(len(iterable) == 2)
        name1, name2 = iterable
        if pd.notnull(name1) and pd.notnull(name2):
            score = jellyfish.jaro_winkler(name1.lower(), name2.lower())
        else:
            score = 0
            name1 = '' if pd.isnull(name1) else name1
            name2 = '' if pd.isnull(name2) else name2
            
        if score > 0.9:
            return name1
        else:
            return ' '.join(filter(None,(name1,name2)))

    return delimiter.join(str(element) for element in iterable if pd.notnull(element) and str(element).strip())

####################
# Pandas functions #
####################

def add_prediction_flag(data, model, features, flag):
    """Adds a column to the DataFrame that contains the predictions of a model

    Parameters
    ----------
    data: pandas.DataFrame
        Input DataFrame
    model: sklearn classifier
        Model to be used for prediction
    features: sklearn.pipeline.Pipeline / numpy.array / pandas.DataFrame
        An sklearn pipeline for feature generation or an array of features
    flag: str
        Name of the flag in the output

    Returns
    -------
    result: pandas.DataFrame
        Version of the input with an additional flag
    """
    data_frame = data.copy()

    # Generate feature array
    if isinstance(features, Pipeline):
        feature_array = features.fit_transform(data)
    else:
        feature_array = features
    
    data_frame[flag] = model.predict(feature_array)
    return data_frame

def concatenate_columns(pandas_df, column_list, delimiter = ' '):
    """Returns a list that contains the collection of concatenated strings in the specified
    columns. Nulls are ignored before concatenating.
    
    TODO: A native pandas implementation might be better 

    Parameters
    ----------
    pandas_df: pandas.DataFrame
        input pandas dataframe
    column_list: list
        list of columns whose contents ought to be concatenated
    delimiter: str, optional
        delimiter for the concatenated strings
    
    Returns
    -------
    list
    """
    # Checking if all columns are present. Ignores columns that are absent
    if not every_a_in_b(column_list, pandas_df.columns):
        warnings.warn('Not all columns specified in column_list are present. Ignoring extra columns.', RuntimeWarning)
    collection_of_cols = [pandas_df[column].fillna('') for column in column_list if column in pandas_df.columns]
    return [concatenate_strings(strings, delimiter) for strings in zip(*collection_of_cols)]

def get_weighted_sum(pandas_df, weights):
    """Returns a numpy array that is weighted sum of the specified columns in a 
    Pandas DataFrame

    Parameters
    ----------
    pandas_df: pandas.DataFrame
        Input Pandas dataframe
    
    weights: dict
        A dict that has the names of the columns as keys and the corresponding values as weights (floats)
    
    Returns
    -------
    numpy.array
    """
    column_names = weights.keys()
    multipliers = np.array(list(weights.values()))
    return pandas_df[column_names].dot(multipliers)

def get_fill_rate(pandas_df, weights):
    """Returns a numpy array that is weighted sum of the fill flags (ie:- whether cell is filled) specified columns in a 
    Pandas DataFrame

    Parameters
    ----------
    pandas_df: pandas.DataFrame
        Input Pandas dataframe
    
    weights: dict
        A dict that has the names of the columns as keys and the corresponding values as weights (floats)
    
    Returns
    -------
    numpy.array
    """
    column_names = weights.keys()
    multipliers = np.array(list(weights.values()))

    return (pandas_df[column_names].notnull()*1).dot(multipliers)
